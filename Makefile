TARGET:=hiber_test
# TODO change to your ARM gcc toolchain path
TOOLCHAIN_ROOT:= /opt/gcc-arm-none-eabi-5
TOOLCHAIN_PATH:=$(TOOLCHAIN_ROOT)/bin
TOOLCHAIN_PREFIX:=arm-none-eabi

# Optimization level, can be [0, 1, 2, 3, s].
OPTLVL:=s
DBG:=-g

STARTUP:=$(CURDIR)/SRC/hardware
LINKER_SCRIPT:=$(CURDIR)/stm32_flash.ld

INCLUDE=-I$(CURDIR)/SRC/hardware
INCLUDE+=-I$(CURDIR)/SRC/
INCLUDE+=-I$(CURDIR)/SRC/Libraries
INCLUDE+=-I$(CURDIR)/SRC/Libraries/CMSIS/Device/ST/STM32F4xx/Include
INCLUDE+=-I$(CURDIR)/SRC/Libraries/CMSIS/Include
INCLUDE+=-I$(CURDIR)/SRC/Libraries/STM32F4xx_StdPeriph_Driver/inc
INCLUDE+=-I$(CURDIR)/SRC/Libraries/usb_cdc_device
INCLUDE+=-I$(CURDIR)/SRC/Libraries/mini_printf
INCLUDE+=-I$(CURDIR)/SRC/Libraries/queue
INCLUDE+=-I$(CURDIR)/SRC/drivers
INCLUDE+=-I$(CURDIR)/SRC/debug


BUILD_DIR = $(CURDIR)/build
BIN_DIR = $(CURDIR)/binary

# vpath is used so object files are written to the current directory instead
# of the same directory as their source files
vpath %.c $(CURDIR)/SRC $(CURDIR)/SRC/Libraries $(CURDIR)/SRC/Libraries/usb_cdc_device\
		  $(CURDIR)/SRC/Libraries/STM32F4xx_StdPeriph_Driver/src \
		  $(CURDIR)/SRC/Libraries/mini_printf $(CURDIR)/SRC/Libraries/queue\
          $(CURDIR)/SRC/Libraries/syscall $(CURDIR)/SRC/hardware \
          $(CURDIR)/SRC/drivers $(CURDIR)/SRC/debug

vpath %.s $(STARTUP)
ASRC=startup_stm32f4xx.s

# Project Source Files
SRC+=stm32f4xx_it.c
SRC+=system_stm32f4xx.c
SRC+=main.c
SRC+=hiber_modem.c
SRC+=task_1KHz.c
SRC+=syscalls.c
SRC+=uart.c
SRC+=timers.c
SRC+=io_port.c
SRC+=usb_debug.c
#SRC+=usb_proto.c
SRC+=profiler.c
SRC+=mini-printf.c
SRC+=queue.c

# Standard Peripheral Source Files
SRC+=misc.c
SRC+=stm32f4xx_exti.c
SRC+=stm32f4xx_usart.c
SRC+=stm32f4xx_adc.c
SRC+=stm32f4xx_rcc.c
SRC+=stm32f4xx_gpio.c
SRC+=stm32f4xx_tim.c
SRC+=stm32f4xx_dma.c
SRC+=stm32f4xx_rng.c

#USB VCP LIB
SRC += tm_stm32f4_usb_vcp.c
SRC += usb_bsp.c
SRC += usb_core.c
SRC += usb_dcd_int.c
SRC += usb_dcd.c
SRC += usbd_cdc_core.c
SRC += usbd_cdc_vcp.c
SRC += usbd_core.c
SRC += usbd_desc.c
SRC += usbd_ioreq.c
SRC += usbd_req.c
SRC += usbd_usr.c

CDEFS=-DUSE_STDPERIPH_DRIVER
CDEFS+=-DSTM32F4XX
CDEFS+=-DSTM32F40_41xxx
CDEFS+=-DHSE_VALUE=8000000
CDEFS+=-D__FPU_PRESENT=1
CDEFS+=-D__FPU_USED=1
CDEFS+=-DARM_MATH_CM4
CDEFS+=-DSTM32F4_DISCOVERY_KIT

MCUFLAGS=-mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -finline-functions -Wdouble-promotion -std=gnu99
COMMONFLAGS=-O$(OPTLVL) $(DBG) -Wall -ffunction-sections -fdata-sections
CFLAGS=$(COMMONFLAGS) $(MCUFLAGS) $(INCLUDE) $(CDEFS)

LDLIBS=-lm -lc -lgcc
LDFLAGS=$(MCUFLAGS) -u _scanf_float -u _printf_float -fno-exceptions -Wl,--gc-sections,-T$(LINKER_SCRIPT),-Map,$(BIN_DIR)/$(TARGET).map

CC=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-gcc
LD=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-gcc
OBJCOPY=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-objcopy
AS=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-as
AR=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-ar
GDB=$(TOOLCHAIN_PATH)/$(TOOLCHAIN_PREFIX)-gdb

OBJ = $(SRC:%.c=$(BUILD_DIR)/%.o)

$(BUILD_DIR)/%.o: %.c
	@echo [CC] $(notdir $<)
	@$(CC) $(CFLAGS) $< -c -o $@

all: $(OBJ)
	@echo [AS] $(ASRC)
	@$(AS) -o $(ASRC:%.s=$(BUILD_DIR)/%.o) $(STARTUP)/$(ASRC)
	@echo [LD] $(TARGET).elf
	@$(CC) -o $(BIN_DIR)/$(TARGET).elf $(LDFLAGS) $(OBJ) $(ASRC:%.s=$(BUILD_DIR)/%.o) $(LDLIBS)
	@echo [HEX] $(TARGET).hex
	@$(OBJCOPY) -O ihex $(BIN_DIR)/$(TARGET).elf $(BIN_DIR)/$(TARGET).hex
	@echo [BIN] $(TARGET).bin
	@$(OBJCOPY) -O binary $(BIN_DIR)/$(TARGET).elf $(BIN_DIR)/$(TARGET).bin

.PHONY: clean

clean:
	@echo [RM] OBJ
	@rm -f $(OBJ)
	@rm -f $(ASRC:%.s=$(BUILD_DIR)/%.o)
	@echo [RM] BIN
	@rm -f $(BIN_DIR)/$(TARGET).elf
	@rm -f $(BIN_DIR)/$(TARGET).hex
	@rm -f $(BIN_DIR)/$(TARGET).bin

flash:
	@st-flash write $(BIN_DIR)/$(TARGET).bin 0x8000000
