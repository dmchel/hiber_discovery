/*
 * main.h
 *
 *  Created on: 18 сент. 2017 г.
 *      Author: dm
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

// Macro to use CCM (Core Coupled Memory) in STM32F4
#define CCM_RAM __attribute__((section(".ccmram")))

//standard lib
#include <math.h>
#include <string.h>
//#include <stdio.h>

//settings defines and constant values
#include "config.h"

//CMSIS
#include "Libraries/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h"

//StdPeriph_Driver
#include "Libraries/STM32F4xx_StdPeriph_Driver/inc/stm32f4xx_usart.h"
#include "Libraries/STM32F4xx_StdPeriph_Driver/inc/stm32f4xx_tim.h"
#include "Libraries/STM32F4xx_StdPeriph_Driver/inc/stm32f4xx_dma.h"
#include "Libraries/STM32F4xx_StdPeriph_Driver/inc/stm32f4xx_rng.h"
#include "Libraries/STM32F4xx_StdPeriph_Driver/inc/stm32f4xx_adc.h"

//usb vsp lib
#include "Libraries/tm_stm32f4_usb_vcp.h"

//lightweight printf implementation
#include "Libraries/mini_printf/mini-printf.h"

#include "Libraries/queue/queue.h"

#include "drivers/io_port.h"
#include "drivers/uart.h"
#include "drivers/timers.h"
#include "debug/usb_debug.h"
//#include "debug/usb_proto.h"
#include "debug/profiler.h"

#include "hiber_modem.h"

void task_1KHz();

int32_t getInternalSensorTemperature();

#endif /* SRC_MAIN_H_ */
