/*
 * uart.h
 *
 *  Created on: 27 нояб. 2017 г.
 *      Author: dm
 */

#ifndef SRC_DEV_UART_H_
#define SRC_DEV_UART_H_

#include <string.h>

typedef enum {
	BR_1200 = 1200,
	BR_2400 = 2400,
	BR_4800 = 4800,
	BR_9600 = 9600,
	BR_19200 = 19200,
	BR_38400 = 38400,
	BR_57600 = 57600,
	BR_115200 = 115200,
	BR_230400 = 230400,
	BR_460800 = 460800,
	BR_921600 = 921600
} UART_BAUDRATES_t;

void initUart();

int uartReadAll(uint8_t* dest, uint16_t maxLen);
void uartWriteByte(uint8_t byte);
void uartWriteStr(uint8_t* str);
void uartWriteArray(uint8_t* data, uint16_t len);

void uartIrqHandler();
void dmaHalfRxHandler();
void dmaFullRxHandler();
void dmaHalfTxHandler();
void dmaFullTxHandler();


#endif /* SRC_DEV_UART_H_ */
