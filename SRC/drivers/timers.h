/*
 * timers.h
 *
 *  Created on: 22 нояб. 2017 г.
 *      Author: leshhev_ds
 */

#ifndef SRC_DEV_TIMERS_H_
#define SRC_DEV_TIMERS_H_

int initTimers(uint32_t base_khz);
void initTimerCCR(uint32_t interval);
void setTimerCCR(uint32_t interval);

void systemTimerIrq();

typedef struct {
	uint32_t init_time;		//время на момент инциализации таймера, мс
	uint32_t timeout;		//таймаут ожидания, мс
} SimpleTimer_t;

void timerInit(SimpleTimer_t *timer);
void timerStart(SimpleTimer_t *timer, uint32_t value);
int timerExpired(SimpleTimer_t *timer);

uint32_t secFromStart();
uint32_t mSecFromStart();
uint32_t mkSecFromStart();

#endif /* SRC_DEV_TIMERS_H_ */
