/*
 * timers.c
 *
 *  Initialization of general purpose timers.
 *  System time counting.
 *
 *  Created on: 22 нояб. 2017 г.
 *      Author: leshchev_ds
 */

#include "main.h"

#define TIMER_PERIOD 84

extern short fMainLoopStep;

static uint32_t sysTick_ms = 0;
static uint32_t sysTick_mks = 0;
static uint32_t sysTick_ns = 0;

static uint32_t baseFrequencyKhz = 1;

static uint32_t getTimerClockFrequency();

/**
 * GP Timers init
 * base_khz - main timer frequency in khz
 * ret 0 - init failed, else - init ok
 */
int initTimers(uint32_t base_khz)
{
	int fInitOk = 0;

	if(base_khz) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);

		//base timer frequency in kHz
		//uint32_t fKHz = (SystemCoreClock / 2) / 1000;
		uint32_t fKHz = getTimerClockFrequency() / 1000;
		if((fKHz % base_khz) == 0) {
			uint16_t prescaler = fKHz / base_khz;
			if((prescaler != 0) && ((prescaler % TIMER_PERIOD) == 0)) {
				prescaler /= TIMER_PERIOD;

				TIM_TimeBaseInitTypeDef TIM_InitStruct;
				TIM_TimeBaseStructInit(&TIM_InitStruct);
				TIM_InitStruct.TIM_Prescaler = prescaler - 1;
				TIM_InitStruct.TIM_Period = TIMER_PERIOD - 1;
				TIM_TimeBaseInit(TIM3, &TIM_InitStruct);

				//TIM_OCInitTypeDef ocInit;
				//TIM_OCStructInit(&ocInit);

				//ocInit.TIM_OCMode = TIM_OCMode_Timing;
				//ocInit.TIM_Pulse = 1000;

				//TIM_OC1Init(TIM3, &ocInit);
				//TIM_CCxCmd(TIM3, TIM_Channel_1, ENABLE);

				//enable counter-overflow interrupt on base_khz
				TIM_ClearITPendingBit(TIM3, TIM_IT_Update | TIM_IT_CC1);
				TIM_ITConfig(TIM3, TIM_IT_Update | TIM_IT_CC1, ENABLE);
				TIM_Cmd(TIM3, ENABLE);
				fInitOk = 1;
				baseFrequencyKhz = base_khz;
			}
			else {
				RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , DISABLE);
			}
		}
		else {
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , DISABLE);
		}
	}

	return fInitOk;
}

/**
 * Init time CCR and enable CC1 interrupt
 */
void initTimerCCR(uint32_t interval)
{
	TIM_OCInitTypeDef ocInit;
	TIM_OCStructInit(&ocInit);

	ocInit.TIM_OCMode = TIM_OCMode_Timing;
	ocInit.TIM_Pulse = interval;

	TIM_OC1Init(TIM3, &ocInit);

	TIM_ClearITPendingBit(TIM3, TIM_IT_Update | TIM_IT_CC1);
	TIM_ITConfig(TIM3, TIM_IT_Update | TIM_IT_CC1, ENABLE);
}

void setTimerCCR(uint32_t interval)
{
	TIM_SetCompare1(TIM3, interval);
}

/**
 * Base timer interrupt handler
 */
void systemTimerIrq()
{
	if(baseFrequencyKhz > 1000) {
		sysTick_ns += (1000000 / baseFrequencyKhz);
		if(sysTick_ns == 1000) {
			sysTick_mks++;
			sysTick_ns = 0;
		}
	}
	else {
		sysTick_mks += (1000 / baseFrequencyKhz);
	}
	if(sysTick_mks == 1000) {
		sysTick_ms++;
		sysTick_mks = 0;
		fMainLoopStep = 1;
	}
}

/**
 * Simple timer initialization.
 * Not necessary to use, you can just
 * call timerStart().
 * But in some cases, it might be useful.
 */
void timerInit(SimpleTimer_t *timer)
{
	if(timer) {
		timer->init_time = 0;
		timer->timeout = 0;
	}
}

/**
 *  Start simple timer for value timeout in ms
 */
void timerStart(SimpleTimer_t *timer, uint32_t value)
{
	if(timer) {
		timer->init_time = sysTick_ms;
		timer->timeout = value;
	}
}

/**
 * Check for timer expire
 */
int timerExpired(SimpleTimer_t *timer)
{
	int res = 1;
	if((timer != 0) && (timer->init_time + timer->timeout > sysTick_ms)) {
		res = 0;
	}
	return res;
}

uint32_t secFromStart()
{
	return (sysTick_ms / 1000);
}

uint32_t mSecFromStart()
{
	return sysTick_ms;
}

uint32_t mkSecFromStart()
{
	return (sysTick_ms * 1000) + sysTick_mks;
}

/**
 * Private methods
 */

static uint32_t getTimerClockFrequency()
{
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);
  uint32_t multiplier = 0;
  if(RCC_Clocks.PCLK1_Frequency == RCC_Clocks.SYSCLK_Frequency) {
	  multiplier = 1;
  }
  else {
	  multiplier = 2;
  }
  return multiplier * RCC_Clocks.PCLK1_Frequency;
}


