/*
 * uart.c
 *
 *  Модуль работы с uart.
 *
 *  Created on: 27 нояб. 2017 г.
 *      Author: dm
 */

#include "../main.h"

#define UART_BUFF_IN_SIZE 128
#define UART_BUFF_OUT_SIZE 128

static CCM_RAM uint8_t uartRxBuffer[UART_BUFF_IN_SIZE];
static CCM_RAM uint8_t uartTxBuffer[UART_BUFF_OUT_SIZE];

static queue_t rxQueue;
static queue_t txQueue;

static void initUSART3();

void initUart()
{
	initUSART3();
	queue_init(&rxQueue, uartRxBuffer, sizeof(uartRxBuffer), 1);
	queue_init(&txQueue, uartTxBuffer, sizeof(uartTxBuffer), 1);
}

/**
 * Прочитать содержимое приемного буфера в dest
 * (если maxLen != 0, то копируется не более maxLen байт)
 * return количество считанных байт
 */
int uartReadAll(uint8_t *dest, uint16_t maxLen)
{
	int res = 0;
	if(!queue_is_empty(&rxQueue)) {
		uint16_t len = rxQueue.count;
		if(maxLen && (maxLen < rxQueue.count)) {
			len = maxLen;
		}
		while(len--) {
			if(queue_get(&rxQueue, dest++)) {
				res++;
			}
		}
	}
	return res;
}

/**
 * Запись байта в буфер передачи
 */
void uartWriteByte(uint8_t byte)
{
	//usb_printf("wr %x \r\n", byte);
	if(USART_GetFlagStatus(USART3, USART_FLAG_TXE)) {
		USART_SendData(USART3, byte);
	}
	//в очередь
	else {
		queue_put(&txQueue, &byte);
	}
}

/**
 * Запись строки в буфер передачи
 */
void uartWriteStr(uint8_t* str)
{
	if(str) {
		size_t len = strlen((char*)str);

		//usb_printf("wr str: ");
		//for(size_t i = 0; i < len; i++) {
		//	usb_printf("%x ", str[i]);
		//}
		//usb_printf("\r\n");

		if(!len) {
			return;
		}

		uint8_t* dest = str;

		if(USART_GetFlagStatus(USART3, USART_FLAG_TXE)) {
			USART_SendData(USART3, str[0]);
			len--;
			dest++;
		}

		while(len--) {
			queue_put(&txQueue, dest++);
		}
	}
}

void uartWriteArray(uint8_t* data, uint16_t len)
{
	if(data && len) {
		if(USART_GetFlagStatus(USART3, USART_FLAG_TXE)) {
			USART_SendData(USART3, data[0]);
			len--;
			data++;
		}

		while(len--) {
			queue_put(&txQueue, data++);
		}
	}
}

/**
 * Обработчик прерываний UART
 */
void uartIrqHandler()
{
	//Прием
	if(USART_GetFlagStatus(USART3, USART_FLAG_RXNE)) {
		//есть место в очереди приема
		if(!queue_is_full(&rxQueue)) {
			uint8_t byte = USART_ReceiveData(USART3);
			queue_put(&rxQueue, &byte);
		}
	}
	//Передача
	if(USART_GetFlagStatus(USART3, USART_FLAG_TXE)) {
		//есть что предавать
		if(!queue_is_empty(&txQueue)) {
			uint8_t byte = 0;
			queue_get(&txQueue, &byte);
			USART_SendData(USART3, byte);
		}
	}
}

void dmaHalfRxHandler()
{

}

void dmaFullRxHandler()
{

}

void dmaHalfTxHandler()
{

}

void dmaFullTxHandler()
{

}

/*
 * Configure USART3(PB10, PB11).
 */
static void initUSART3() {
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11; //| GPIO_Pin_13 | GPIO_Pin_14;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_USART3);
  //GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_USART3);

  USART_InitStruct.USART_BaudRate = BR_19200;
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
  USART_InitStruct.USART_StopBits = USART_StopBits_1;
  USART_InitStruct.USART_Parity = USART_Parity_No;
  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
  USART_Init(USART3, &USART_InitStruct);
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART3, USART_IT_TC, ENABLE);
  //USART_ITConfig(USART3, USART_IT_CTS, ENABLE);
  USART_Cmd(USART3, ENABLE);
}
