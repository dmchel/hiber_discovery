/*
 * io_port.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: dm
 */

#ifndef SRC_DEV_IO_PORT_H_
#define SRC_DEV_IO_PORT_H_

void initIO(void);

typedef struct {
	GPIO_TypeDef *port;
	uint16_t pin;
} IO_Pin_t;

/**
 * IO pin names
 */
typedef enum {
	BOARD_LED_LD3,
	BOARD_LED_LD4,
	BOARD_LED_LD5,
	BOARD_LED_LD6,
	BOARD_USER_BUTTON,
	HIBER_WAKE_UP,
	HIBER_RESET
} IO_PinName_t;

extern IO_Pin_t pinMap[];
extern const int pinMapSize;

inline void __attribute__ ((always_inline)) setPinTrue(IO_PinName_t name)
{
	if(name < pinMapSize) {
		GPIO_SetBits(pinMap[name].port, pinMap[name].pin);
	}
}

inline void __attribute__ ((always_inline)) setPinFalse(IO_PinName_t name)
{
	if(name < pinMapSize) {
		GPIO_ResetBits(pinMap[name].port, pinMap[name].pin);
	}
}

inline void __attribute__ ((always_inline)) setPin(IO_PinName_t name, uint32_t state)
{
	if(name < pinMapSize) {
		GPIO_WriteBit(pinMap[name].port, pinMap[name].pin, state & 0x1);
	}
}

inline int __attribute__ ((always_inline)) getPin(IO_PinName_t name)
{
	int state = -1;
	if(name < pinMapSize) {
		state = GPIO_ReadInputDataBit(pinMap[name].port, pinMap[name].pin);
	}
	return state;
}

#endif /* SRC_DEV_IO_PORT_H_ */
