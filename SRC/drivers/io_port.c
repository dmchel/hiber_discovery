/*
 * io_port.c
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: dm
 */

#include "main.h"

IO_Pin_t pinMap[] = {
		{GPIOD, GPIO_Pin_12},		//BOARD_LED_LD3
		{GPIOD, GPIO_Pin_13},		//BOARD_LED_LD4
		{GPIOD, GPIO_Pin_14},		//BOARD_LED_LD5
		{GPIOD, GPIO_Pin_15},		//BOARD_LED_LD6
		{GPIOA, GPIO_Pin_0},		//BOARD_USER_BUTTON
		{GPIOA, GPIO_Pin_1},		//HIBER_WAKE_UP
		{GPIOA, GPIO_Pin_3}			//HIBER_RESET
};

const int pinMapSize = sizeof(pinMap) / sizeof(IO_Pin_t);

static void initLedPins();
static void initPin(IO_PinName_t name, GPIOMode_TypeDef mode, GPIOOType_TypeDef type, GPIOPuPd_TypeDef pull, GPIOSpeed_TypeDef speed, uint32_t state);

void initIO(void)
{
	initLedPins();
}

static void initLedPins()
{
	initPin(BOARD_LED_LD3, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
	initPin(BOARD_LED_LD4, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
	initPin(BOARD_LED_LD5, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
	initPin(BOARD_LED_LD6, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
	initPin(BOARD_USER_BUTTON, GPIO_Mode_IN, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, 0);
	initPin(HIBER_WAKE_UP, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
	initPin(HIBER_RESET, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_50MHz, 0);
}

static void initPin(IO_PinName_t name, GPIOMode_TypeDef mode, GPIOOType_TypeDef type, GPIOPuPd_TypeDef pull, GPIOSpeed_TypeDef speed, uint32_t state)
{
	if(name < pinMapSize) {
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_StructInit(&GPIO_InitStruct);

		uint8_t rcc_offset = ((uint32_t) pinMap[name].port - GPIOA_BASE) / 0x0400;

		if(rcc_offset < 11) {
			RCC_AHB1PeriphClockCmd((RCC_AHB1Periph_GPIOA << rcc_offset), ENABLE);
		}

		GPIO_InitStruct.GPIO_Pin = pinMap[name].pin;
		GPIO_InitStruct.GPIO_Mode = mode;
		GPIO_InitStruct.GPIO_Speed = speed;
		GPIO_InitStruct.GPIO_OType = type;
		GPIO_InitStruct.GPIO_PuPd = pull;

		GPIO_Init(pinMap[name].port, &GPIO_InitStruct);
		GPIO_WriteBit(pinMap[name].port, pinMap[name].pin, state & 0x1);

	}
}

