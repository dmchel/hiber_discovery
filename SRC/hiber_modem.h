/*
 * hiber_modem.h
 *
 * Hiber LPGAN modem API
 *
 * The application programming interface (API) of the Hiber LPGAN modem is text-based
 * command/response serial protocol with a common message syntax. Each message is terminated
 * with a CRLF (‘\r\n’) character pair.
 *
 *  Created on: 16 июл. 2019 г.
 *  	Author: leshchev
 */

#ifndef SRC_HIBER_MODEM_H_
#define SRC_HIBER_MODEM_H_

// This is the maximum amount of bytes you
// send through the modem
#define HIBER_MAX_DATA_LEN (144)

/**
 * Hiber modem state.
 */
typedef enum {
	HIBER_STATE_UNKNOWN,
	HIBER_STATE_OFFLINE,
	HIBER_STATE_FREE,
	HIBER_STATE_WAIT_DATA,
	HIBER_STATE_SLEEP
} HiberState_t;

/**
 * Hiber modem response codes.
 */
typedef enum {
	HIBER_RESPONSE_UNKNOWN_COMMAND = 425,
	HIBER_RESPONSE_WRONG_ARGUMENTS = 426,
	HIBER_RESPONSE_OK = 600,
	HIBER_RESPONSE_STARTING_TO_SLEEP = 602,
	HIBER_RESPONSE_CANNOT_SLEEP_WKUP0_HIGH = 603,
	HIBER_RESPONSE_INVALID_INPUT = 625,
	HIBER_RESPONSE_UNKNOWN_COMMAND_HELP = 626,
	HIBER_RESPONSE_ERROR_GPS_IS_DISABLED = 632,
	HIBER_RESPONSE_ERROR_GPS_IS_ENABLED = 633,
	HIBER_RESPONSE_COMMAND_NOT_IMPLEMENTED = 634,
	HIBER_RESPONSE_NOT_GOING_TO_SLEEP_SOON_WAKEUP = 635,
	HIBER_RESPONSE_GENERIC_ERROR = 636,

	//invalid response codes (parser returns)
	HIBER_RESPONSE_TOO_SHORT = 950, 				// Unable to parse response (too short to be valid)
	HIBER_RESPONSE_NO_END = 951, 					// Unable to parse response, because it had no termination character
	HIBER_RESPONSE_STRING_ARG = 952, 				// Unable to parse a string in the response
	HIBER_RESPONSE_NO_BEGIN = 953, 					// Unable to parse response (did not start with API( )
	HIBER_RESPONSE_DEVICE_JUST_BOOTED = 954, 		// Received booting message

} HiberResponseCode_t;

void hiberInit();
void hiberProcess();

HiberState_t hbState();

/**
 * Weak functions. Realization must be provided by user.
 */
void hbWrite(uint8_t* str);
int hbRead(uint8_t* buffer, size_t maxLen);
void hbWakeUp(uint8_t flag);
void hbReset(uint8_t flag);
void hbSetOnlineLed(uint8_t flag);
void hbSetDataLed(uint8_t flag);

/**
 * Hiber version info
 */
void hbGetBuildVersion(char* const result, int maxLen);
void hbGetBuildDate(char* const result, int maxLen);

/**
 *  Hiber API commands functions
 */
void hbSetGpsMode(uint8_t flag, uint16_t* responseCode);
void hbDoGpsFix(uint16_t* responseCode);
void hbGetLocation(char* const result, int maxLen, uint16_t* responseCode);
void hbSetLocation(char* longtitude_str, char* latitude_str, uint16_t* responseCode);
void hbGetDatetime(char* const result, int maxLen, uint16_t* responseCode);
void hbSetDatetime(char* datetime_str, uint16_t* responseCode);
void hbSetPayload(uint32_t size, uint8_t* data, uint16_t* responseCode);
void hbGetNextWakeupTime(char* const result, int maxLen, uint16_t* responseCode);
void hbGoToSleep(uint16_t* responseCode);

//for latest API versions
void hbGetModemInfo(char* const result, int maxLen, uint16_t* responseCode);
void hbGetFirmwareVersion(char* const result, int maxLen, uint16_t* responseCode);


#endif /* SRC_HIBER_MODEM_H_ */
