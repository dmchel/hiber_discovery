/*
 * hiber_modem.c
 *
 * Hiber LPGAN modem API
 *
 * The application programming interface (API) of the Hiber LPGAN modem is text-based
 * command/response serial protocol with a common message syntax. Each message is terminated
 * with a CRLF (‘\r\n’) character pair.
 *
 *  Created on: 16 июл. 2019 г.
 *      Author: dm
 */

#include "main.h"

/**
 * Internal error codes.
 */
typedef enum {
	HB_NO_ERROR = 0,
	HB_ERROR_INVALID_INPUT = -1,
	HB_ERROR_OVERFLOW = -2
} HBInternalErrorCode_t;

#define HIBER_INPUT_BUFFER_SIZE 128
#define HIBER_OUTPUT_BUFFER_SIZE 128
#define HIBER_ARGUMENT_MAX_SIZE 64
#define HIBER_RESPONSE_TIMEOUT 5000
#define HIBER_SLEEP_TIMEOUT 120000
#define HIBER_DATA_LED_TIMEOUT 500

static HiberState_t modemState;
static uint8_t fGoToSleep;
//IO buffers
static uint8_t outputData[HIBER_OUTPUT_BUFFER_SIZE];
static uint8_t inputData[HIBER_INPUT_BUFFER_SIZE];
//last hiber response
static uint8_t hiberResponseStr[HIBER_INPUT_BUFFER_SIZE];
static uint8_t* hiberResponsePtr = hiberResponseStr;

static uint8_t* dataToSendPtr;
static uint32_t dataToSendSize;
static uint16_t* responseCodePtr;
static uint8_t* responseDestPtr;

static SimpleTimer_t responseTimer;
static SimpleTimer_t sleepTimer;
static SimpleTimer_t dataLedTimer;

static void handleData(int count);
static void onUnknownState(HiberState_t* state);
static void onOfflineState(HiberState_t* state);
static void onWaitDataState(HiberState_t* state);
static void onFreeState(HiberState_t* state);
static void onSleepState(HiberState_t* state);

static HBInternalErrorCode_t writeCommand(const char* body, const char* arg1, const char* arg2);
static HiberResponseCode_t parseResponse(const uint8_t* response, uint8_t* result);
static HiberResponseCode_t parseResponseInternal(const uint8_t* response, uint8_t* result);
static uint16_t stringToCode(const char* str);

/**
 * Initialization of Hiber modem connection
 */
void hiberInit()
{
	modemState = HIBER_STATE_UNKNOWN;
	hiberResponsePtr = hiberResponseStr;
	dataToSendPtr = NULL;
	dataToSendSize = 0;
	responseCodePtr = NULL;
	responseDestPtr = NULL;
	fGoToSleep = 0;
}

/**
 * Handle HiberBand modem connection
 */
void hiberProcess()
{
	int byte_count = hbRead(inputData, HIBER_INPUT_BUFFER_SIZE);
	if(byte_count) {
		hbSetDataLed(1);
		timerStart(&dataLedTimer, HIBER_DATA_LED_TIMEOUT);
		handleData(byte_count);
	}

	if(timerExpired(&dataLedTimer)) {
		hbSetDataLed(0);
	}

	switch(modemState) {
	case HIBER_STATE_UNKNOWN:
		onUnknownState(&modemState);
		break;
	case HIBER_STATE_OFFLINE:
		onOfflineState(&modemState);
		break;
	case HIBER_STATE_WAIT_DATA:
		onWaitDataState(&modemState);
		break;
	case HIBER_STATE_FREE:
		onFreeState(&modemState);
		break;
	case HIBER_STATE_SLEEP:
		onSleepState(&modemState);
		break;
	default:
		break;
	}
}

/**
 * Hiber modem state
 */
HiberState_t hbState()
{
	return modemState;
}

void __attribute__((weak)) hbWrite(uint8_t* str)
{
	(void) str;
}

int __attribute__((weak)) hbRead(uint8_t* buffer, size_t maxLen)
{
	(void) buffer;
	(void) maxLen;
	return 0;
}

void __attribute__((weak)) hbWakeUp(uint8_t flag)
{
	(void) flag;
}

void __attribute__((weak)) hbReset(uint8_t flag)
{
	(void) flag;
}

void __attribute__((weak)) hbSetOnlineLed(uint8_t flag)
{
	(void) flag;
}

void __attribute__((weak)) hbSetDataLed(uint8_t flag)
{
	(void) flag;
}

void hbGetBuildVersion(char* const result, int maxLen)
{
	responseDestPtr = (uint8_t*) result;
}

void hbGetBuildDate(char* const result, int maxLen)
{
	responseDestPtr = (uint8_t*) result;
}

void hiberSetGpsMode(uint8_t flag, uint16_t* responseCode)
{
	responseCodePtr = responseCode;
	if(flag) {
		writeCommand("set_gps_mode", "true", NULL);
	}
	else {
		writeCommand("set_gps_mode", "false", NULL);
	}
}

void hiberDoGpsFix(uint16_t* responseCode)
{
	responseCodePtr = responseCode;
	writeCommand("do_gps_fix", NULL, NULL);
}

void hbGetLocation(char* const result, int maxLen, uint16_t* responseCode)
{
	//return API(600: 51.1013; 14.3233; 12; 60)\r\n
	if(maxLen < 30) {
		return;
	}
	responseDestPtr = (uint8_t*) result;
	responseCodePtr = responseCode;
	writeCommand("get_location", NULL, NULL);
}

void hbSetLocation(char* longtitude_str, char* latitude_str, uint16_t* responseCode)
{
	responseCodePtr = responseCode;
	writeCommand("set_location", longtitude_str, latitude_str);
}

void hbGetDatetime(char* const result, int maxLen, uint16_t* responseCode)
{
	//return API(600: 2017-06-14T15:28:06Z)\r\n
	if(maxLen < 20) {
		return;
	}
	responseDestPtr = (uint8_t*) result;
	responseCodePtr = responseCode;
	writeCommand("get_datetime", NULL, NULL);
}

void hbSetDatetime(char* datetime_str, uint16_t* responseCode)
{
	responseCodePtr = responseCode;
	writeCommand("set_datetime", datetime_str, NULL);
}

void hbSetPayload(uint32_t size, uint8_t* data, uint16_t* responseCode)
{
	if(size > HIBER_MAX_DATA_LEN) {
		return;
	}
	char size_str[16];
	snprintf(size_str, 16, "%d");
	responseCodePtr = responseCode;
	writeCommand("set_payload", size_str, NULL);
	dataToSendPtr = data;
	dataToSendSize = size;
}

void hbGetNextWakeupTime(char* const result, int maxLen, uint16_t* responseCode)
{
	if(maxLen < 20) {
		return;
	}
	responseDestPtr = (uint8_t*) result;
	responseCodePtr = responseCode;
	writeCommand("get_next_wakeup_time", NULL, NULL);
}

void hbGoToSleep(uint16_t* responseCode)
{
	hbWakeUp(0);
	responseCodePtr = responseCode;
	writeCommand("go_to_sleep", NULL, NULL);
	timerStart(&sleepTimer, HIBER_SLEEP_TIMEOUT);
	fGoToSleep = 1;
}

void hbGetModemInfo(char* const result, int maxLen, uint16_t* responseCode)
{
	if(maxLen < 32) {
		return;
	}
	responseDestPtr = (uint8_t*) result;
	responseCodePtr = responseCode;
	writeCommand("get_modem_info", NULL, NULL);
}

void hbGetFirmwareVersion(char* const result, int maxLen, uint16_t* responseCode)
{
	if(maxLen < 32) {
		return;
	}
	responseDestPtr = (uint8_t*) result;
	responseCodePtr = responseCode;
	writeCommand("get_firmware_version", NULL, NULL);
}

/**
 * Private section
 */

static void handleData(int count)
{
	for(int i = 0; i < count; i++) {
		*hiberResponsePtr++ = inputData[i];
		if(inputData[i] == '\n') {
			//terminate string
			*hiberResponsePtr = 0;
			usb_printf("hiber modem: %s", hiberResponseStr);
			//parse response
			uint16_t code = parseResponse(hiberResponseStr, responseDestPtr);
			usb_printf("code: %d\r\n", code);
			if(code == HIBER_RESPONSE_DEVICE_JUST_BOOTED) {
				hbSetOnlineLed(1);
				usb_printf("Hiber modem just booted.\r\n");
			}
			if(responseCodePtr != NULL) {
				*responseCodePtr = code;
				//time to sleep
				if((fGoToSleep == 1) && (*responseCodePtr == HIBER_RESPONSE_STARTING_TO_SLEEP)) {
					fGoToSleep = 0;
					modemState = HIBER_STATE_SLEEP;
				}
				//modem is online
				if(((modemState == HIBER_STATE_OFFLINE) || (modemState == HIBER_STATE_WAIT_DATA))
				   && (*responseCodePtr < HIBER_RESPONSE_TOO_SHORT)) {
					hbSetOnlineLed(1);
					modemState = HIBER_STATE_FREE;
				}
				responseCodePtr = NULL;
				responseDestPtr = NULL;
			}
			//set response pointer on start of the response string
			hiberResponsePtr = hiberResponseStr;
		}
	}
}

static void onUnknownState(HiberState_t* state)
{
	hbWakeUp(1);
	*state = HIBER_STATE_WAIT_DATA;
	timerStart(&responseTimer, HIBER_RESPONSE_TIMEOUT);
}

static void onOfflineState(HiberState_t* state)
{
	//intentionally empty for now
}

static void onWaitDataState(HiberState_t* state)
{
	if(timerExpired(&responseTimer)) {
		*state = HIBER_STATE_OFFLINE;
		hbSetOnlineLed(0);
		hbWakeUp(0);
	}
}

static void onFreeState(HiberState_t* state)
{
	//intentionally empty for now
}

static void onSleepState(HiberState_t* state)
{
	if(timerExpired(&sleepTimer)) {
		hbWakeUp(1);
		*state = HIBER_STATE_WAIT_DATA;
		timerStart(&responseTimer, HIBER_RESPONSE_TIMEOUT);
	}
}

static HBInternalErrorCode_t writeCommand(const char* body, const char* arg1, const char* arg2)
{
	uint8_t* out = outputData;
	int bodyLen, arg1Len = 0, arg2Len = 0;
	//copy command body to output buffer
	if(body) {
		bodyLen = strlen(body);
		//total len == body + ()\r\n\0 - without arguments
		if(bodyLen + 5 > HIBER_OUTPUT_BUFFER_SIZE) {
			return HB_ERROR_OVERFLOW;
		}
		strcpy((char*)out, body);
	}
	else {
		return HB_ERROR_INVALID_INPUT;
	}

	out += bodyLen;
	//open bracket
	*out++ = '(';

	if(arg1) {
		*out++ = '"';
		arg1Len = strlen(arg1);
		//total len == body + (\"+ arg1 +\")\r\n\0
		if(bodyLen + arg1Len + 7 > HIBER_OUTPUT_BUFFER_SIZE) {
			return HB_ERROR_OVERFLOW;
		}
		strcpy((char*)out, arg1);
		out += arg1Len;
		*out++ = '"';
	}

	if(arg2) {
		*out++ = ',';
		*out++ = '"';
		arg2Len = strlen(arg2);
		//total len == body + (\"+ arg1 +\" + ,\" + arg2 + \" + )\r\n\0
		if(bodyLen + arg1Len + arg2Len + 10 > HIBER_OUTPUT_BUFFER_SIZE) {
			return HB_ERROR_OVERFLOW;
		}
		strcpy((char*)out, arg2);
		out += arg2Len;
		*out++ = '"';
	}

	//close bracket
	*out++ = ')';
	//add CR and LF
	*out++ = '\r';
	*out++ = '\n';
	//add string terminator
	*out = '\0';

	//send all output data to modem
	hbWrite(outputData);
	//change modem state
	if(modemState != HIBER_STATE_UNKNOWN) {
		modemState = HIBER_STATE_WAIT_DATA;
	}
	//start response timer
	timerStart(&responseTimer, HIBER_RESPONSE_TIMEOUT);

	return HB_NO_ERROR;
}

static HiberResponseCode_t parseResponse(const uint8_t* response, uint8_t* result)
{
	size_t responseLen = strlen((const char*) response);
	//response is too short
	if(responseLen < 7) {
		return HIBER_RESPONSE_TOO_SHORT;
	}
	//check if it Hiber Boot response message
	char* start = strchr((const char*) response, 'H');
	if((start != NULL) && (strlen(start) >= 9)) {
		if(strncmp(start, "Hiber API", 9) == 0) {
			return HIBER_RESPONSE_DEVICE_JUST_BOOTED;
		}
	}
	//lookup for "API(" string
	start = strchr((const char*) response, 'A');
	if(start != NULL) {
		return parseResponseInternal((const uint8_t*) start, result);
	}
	else {
		return HIBER_RESPONSE_NO_BEGIN;
	}
}

static HiberResponseCode_t parseResponseInternal(const uint8_t* response, uint8_t* result)
{
	size_t responseLen = strlen((const char*) response);
	if(responseLen < 7) {
		return HIBER_RESPONSE_TOO_SHORT;
	}
	if(strncmp((const char*) response, "API(", 4) == 0) {
		uint16_t code = stringToCode((const char*)(response + 4));
		char* arg_start = strchr((const char*) response, ':');
		if(arg_start != NULL) {
			arg_start++;
			char* arg_end = strchr((const char*) response, ')');
			if((arg_end != NULL) && (arg_end > arg_start) && (result != NULL)) {
				while(arg_start != arg_end) {
					*result++ = *arg_start++;
				}
			}
		}
		return code;
	}
	else {
		return HIBER_RESPONSE_NO_BEGIN;
	}
}

static uint16_t stringToCode(const char* str)
{
	return ((str[0] - 0x30) * 100 + (str[1] - 0x30) * 10 + (str[2] - 0x30));
}
