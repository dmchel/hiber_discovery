/*
 * usb_debug.h
 *
 *  Created on: 25 окт. 2017 г.
 *      Author: dm
 */

#ifndef SRC_DEBUG_USB_DEBUG_H_
#define SRC_DEBUG_USB_DEBUG_H_

#define DEBUG_BUFFER_SIZE 256

int usb_printf(const char *fmt, ...);

void initUsbDebug();
void debugProcess();

int checkUsbConnection();


#endif /* SRC_DEBUG_USB_DEBUG_H_ */
