/*
 * usb_debug.c
 *
 * Usb print debug
 *
 *  Created on: 25 окт. 2017 г.
 *      Author: dm
 */

#include "../main.h"

#define TEST_BUFFER_SIZE 64

static CCM_RAM uint8_t receiveBuffer[DEBUG_BUFFER_SIZE];
static CCM_RAM char dbgBuffer[DEBUG_BUFFER_SIZE];
static short fEchoMode = 0;
static SimpleTimer_t dbgTimer;
static uint32_t DEBUG_PERIOD_MS = 50;

static uint8_t testBuffer[TEST_BUFFER_SIZE];
static uint16_t testCode;

static void usbReceiveData(uint8_t *data, uint8_t len);
static void dataHandler(uint8_t byte);

/**
 * Formated ouput via usb
 * ret count of printed symbols,
 * or -1 if usb output is not available
 */
int usb_printf(const char *fmt, ...)
{
	int cnt = 0;
	/*if(TM_USB_VCP_GetStatus() != TM_USB_VCP_CONNECTED) {
		return -1;
	}*/

	va_list va;
	va_start(va, fmt);
	cnt = mini_vsnprintf(dbgBuffer, sizeof(dbgBuffer), fmt, va);
	va_end(va);
	TM_USB_VCP_Puts(dbgBuffer);

	return cnt;
}

void initUsbDebug()
{
	TM_USB_VCP_Init();
	//usbProtoInit();
	timerStart(&dbgTimer, DEBUG_PERIOD_MS);
}

int checkUsbConnection()
{
	int rs = 0;
	if(TM_USB_VCP_GetStatus() == TM_USB_VCP_CONNECTED) {
		rs = 1;
	}
	return rs;
}

void debugProcess()
{
	if(timerExpired(&dbgTimer)) {

		uint8_t cnt = 0;
		uint8_t c = 0;

		//get all available data from VCP
		while(TM_USB_VCP_Getc(&c) == TM_USB_VCP_DATA_OK)
		{
			if(fEchoMode) {
				usb_printf("%c", c);
			}
			receiveBuffer[cnt++] = c;
			//oops, that's not good
			if(cnt == DEBUG_BUFFER_SIZE) {
				break;
			}
		}

		//we have some new data to process
		if(cnt) {
			if(fEchoMode) {
				usb_printf("\r\n");
			}
			usbReceiveData(receiveBuffer, cnt);
		}

		timerStart(&dbgTimer, DEBUG_PERIOD_MS);
	}
}

/**
 * Private section
 */

/**
 *	@brief usbReceiveData
 *	New data for handling
 */
static void usbReceiveData(uint8_t *data, uint8_t len)
{
	 //побайтный разбор данных
	while(len--) {
		uint8_t c = *data++;
		dataHandler(c);
	}
}

/**
 *  Handles incoming bytes
 */
static void dataHandler(uint8_t byte)
{
	if(byte == '1') {
		usb_printf("1: Internal temperature: %d ºC\r\n", getInternalSensorTemperature());
	}
	else if(byte == '2') {
		usb_printf("2: Hiber modem state: %d\r\n", hbState());
	}
	else if(byte == '3') {
		usb_printf("Last Hiber parsed response: code=%d result=%s\r\n", testCode, testBuffer);
	}
	else if(byte == 'i') {
		usb_printf("get_modem_info()\r\n");
		hbGetModemInfo(testBuffer, TEST_BUFFER_SIZE, &testCode);
	}
	else if(byte == 'f') {
		usb_printf("get_firmware_version()\r\n");
		hbGetFirmwareVersion(testBuffer, TEST_BUFFER_SIZE, &testCode);
	}
	else if(byte == 'd') {
		usb_printf("get_datetime()\r\n");
		hbGetDatetime(testBuffer, TEST_BUFFER_SIZE, &testCode);
	}
	else if(byte == 'w') {
		usb_printf("get_next_wakeup_time()\r\n");
		hbGetNextWakeupTime(testBuffer, TEST_BUFFER_SIZE, &testCode);
	}
	else if(byte == 'p') {
		usb_printf("set_payload(12)\r\n");
		hbSetPayload(12, testBuffer, &testCode);
	}
	else if(byte == 'o') {
		usb_printf("set_data: Hello World!\r\n");
		uartWriteStr("Hello World!");
	}
	else if(byte == 's') {
		usb_printf("set_location(\"56.3139\", \"44.0689\")\r\n");
		hbSetLocation("56.3139", "44.0689", &testCode);
	}
	else if(byte == 't') {
		usb_printf("set_datetime()\r\n");
		hbSetDatetime("2019-10-21T00:39:10Z", &testCode);
	}
	else if(byte == 'g') {
		usb_printf("go_to_sleep()\r\n");
		hbGoToSleep(&testCode);
	}
}

