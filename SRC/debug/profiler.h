/*
 * profiler.h
 *
 *  Created on: 25 апр. 2018 г.
 *      Author: leshhev_ds
 */

#ifndef SRC_DEBUG_PROFILER_H_
#define SRC_DEBUG_PROFILER_H_

typedef struct {
	uint32_t startTicks;
	uint32_t result;
} ProfileInfo_t;

#define INIT_PROFILER_INFO {0, 0}

void initProfiler();
void startProfiler(ProfileInfo_t *info);
void stopProfiler(ProfileInfo_t *info);

#endif /* SRC_DEBUG_PROFILER_H_ */
