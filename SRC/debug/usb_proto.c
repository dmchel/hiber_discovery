/*
 * usb_proto.c
 *
 * Модуль реализации протокола ИЛВ для
 * связи с ПК по интерфейсу USB STM Virtual Com Port (VCP)
 *
 *  Created on: 14 янв. 2018 г.
 *      Author: dm
 */

#include <stdlib.h>
#include <string.h>
//added for typedefs uintx_t
#include "../Libraries/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h"

#include "usb_proto.h"

enum StuffBytes {
	FSTART = 0xC0,
	FESC = 0xDB,
	TFSTART = 0xDC,
	TFEND = 0xDD
};

#define CRC16_CCITT_INIT	0xffff  /* Initial checksum value */

/*
 * Checksum lookup table, from RFC-1662.
 * CRC-CCITT = x^16 + x^12 + x^5 + 1
 */
static const unsigned short poly_tab [256] = {
	0x0000,	0x1189,	0x2312,	0x329b,	0x4624,	0x57ad,	0x6536,	0x74bf,
	0x8c48,	0x9dc1,	0xaf5a,	0xbed3,	0xca6c,	0xdbe5,	0xe97e,	0xf8f7,
	0x1081,	0x0108,	0x3393,	0x221a,	0x56a5,	0x472c,	0x75b7,	0x643e,
	0x9cc9,	0x8d40,	0xbfdb,	0xae52,	0xdaed,	0xcb64,	0xf9ff,	0xe876,
	0x2102,	0x308b,	0x0210,	0x1399,	0x6726,	0x76af,	0x4434,	0x55bd,
	0xad4a,	0xbcc3,	0x8e58,	0x9fd1,	0xeb6e,	0xfae7,	0xc87c,	0xd9f5,
	0x3183,	0x200a,	0x1291,	0x0318,	0x77a7,	0x662e,	0x54b5,	0x453c,
	0xbdcb,	0xac42,	0x9ed9,	0x8f50,	0xfbef,	0xea66,	0xd8fd,	0xc974,
	0x4204,	0x538d,	0x6116,	0x709f,	0x0420,	0x15a9,	0x2732,	0x36bb,
	0xce4c,	0xdfc5,	0xed5e,	0xfcd7,	0x8868,	0x99e1,	0xab7a,	0xbaf3,
	0x5285,	0x430c,	0x7197,	0x601e,	0x14a1,	0x0528,	0x37b3,	0x263a,
	0xdecd,	0xcf44,	0xfddf,	0xec56,	0x98e9,	0x8960,	0xbbfb,	0xaa72,
	0x6306,	0x728f,	0x4014,	0x519d,	0x2522,	0x34ab,	0x0630,	0x17b9,
	0xef4e,	0xfec7,	0xcc5c,	0xddd5,	0xa96a,	0xb8e3,	0x8a78,	0x9bf1,
	0x7387,	0x620e,	0x5095,	0x411c,	0x35a3,	0x242a,	0x16b1,	0x0738,
	0xffcf,	0xee46,	0xdcdd,	0xcd54,	0xb9eb,	0xa862,	0x9af9,	0x8b70,
	0x8408,	0x9581,	0xa71a,	0xb693,	0xc22c,	0xd3a5,	0xe13e,	0xf0b7,
	0x0840,	0x19c9,	0x2b52,	0x3adb,	0x4e64,	0x5fed,	0x6d76,	0x7cff,
	0x9489,	0x8500,	0xb79b,	0xa612,	0xd2ad,	0xc324,	0xf1bf,	0xe036,
	0x18c1,	0x0948,	0x3bd3,	0x2a5a,	0x5ee5,	0x4f6c,	0x7df7,	0x6c7e,
	0xa50a,	0xb483,	0x8618,	0x9791,	0xe32e,	0xf2a7,	0xc03c,	0xd1b5,
	0x2942,	0x38cb,	0x0a50,	0x1bd9,	0x6f66,	0x7eef,	0x4c74,	0x5dfd,
	0xb58b,	0xa402,	0x9699,	0x8710,	0xf3af,	0xe226,	0xd0bd,	0xc134,
	0x39c3,	0x284a,	0x1ad1,	0x0b58,	0x7fe7,	0x6e6e,	0x5cf5,	0x4d7c,
	0xc60c,	0xd785,	0xe51e,	0xf497,	0x8028,	0x91a1,	0xa33a,	0xb2b3,
	0x4a44,	0x5bcd,	0x6956,	0x78df,	0x0c60,	0x1de9,	0x2f72,	0x3efb,
	0xd68d,	0xc704,	0xf59f,	0xe416,	0x90a9,	0x8120,	0xb3bb,	0xa232,
	0x5ac5,	0x4b4c,	0x79d7,	0x685e,	0x1ce1,	0x0d68,	0x3ff3,	0x2e7a,
	0xe70e,	0xf687,	0xc41c,	0xd595,	0xa12a,	0xb0a3,	0x8238,	0x93b1,
	0x6b46,	0x7acf,	0x4854,	0x59dd,	0x2d62,	0x3ceb,	0x0e70,	0x1ff9,
	0xf78f,	0xe606,	0xd49d,	0xc514,	0xb1ab,	0xa022,	0x92b9,	0x8330,
	0x7bc7,	0x6a4e,	0x58d5,	0x495c,	0x3de3,	0x2c6a,	0x1ef1,	0x0f78,
};

/*
 * Calculate a new sum given the current sum and the new data.
 * Use 0xffff as the initial sum value.
 * Do not forget to invert the final checksum value.
 */
static unsigned short crc16_ccitt (unsigned short sum, unsigned const char *buf, unsigned short len)
{
	if (len) do {
		sum = (sum >> 8) ^ poly_tab [*buf++ ^ (unsigned char) sum];
	}
	while (--len);
	return sum;
}

static unsigned short crc16_ccitt_byte (unsigned short sum, unsigned char byte)
{
	sum = (sum >> 8) ^ poly_tab [byte ^ (unsigned char) sum];
	return sum;
}

static USBProtoStatus_t protoStatus;
static uint8_t packBuffer[USB_PROTO_BUFFER_SIZE];
static SerialPacket currPacket;
static ReceiverState rxState;
static uint8_t crcByteNum;
static uint16_t currCrc;

static void dataHandler(uint8_t c);
static ReceiverState checkStartMark(uint8_t byte);
static ReceiverState checkAddress(uint8_t byte);
static ReceiverState checkCmd(uint8_t byte);
static ReceiverState checkLen(uint8_t byte);
static ReceiverState checkData(uint8_t byte);
static ReceiverState doSomeStuff(ReceiverState state, uint8_t byte);
static ReceiverState checkCrc(uint8_t byte);
static ReceiverState finalCrcCheck(uint8_t byte);

static uint16_t computeCRC(const SerialPacket *pack);
static void resetRxBuffer();
static void resetCrc();
static uint8_t tryToStuffByte(uint8_t byte);


void SP_Init(SerialPacket *pack, uint8_t addr, uint8_t cmd, uint8_t len)
{
    if(!pack) {
    	return;
    }

	pack->address = addr;
    pack->command = cmd;
    pack->length = len;
    pack->data = 0;
    pack->head = 0;
}

void SP_AttachBuffer(SerialPacket *pack, uint8_t *buffer, uint8_t size)
{
	if(pack && buffer) {
		if(pack->length > size) {
			pack->length = size;
		}
		pack->data = buffer;
		pack->head = buffer;
	}
}

void SP_Clear(SerialPacket *pack)
{
	if(pack) {
		pack->address = 0;
		pack->command = 0;
		pack->length = 0;
		pack->data = 0;
		pack->head = 0;
	}
}

int SP_isValid(SerialPacket *pack)
{
	int fValid = 1;
	if(pack) {
		if((pack->address & 0x80) || (pack->command & 0x80)) {
			fValid = 0;
		}
	}
	else {
		fValid = 0;
	}
	return fValid;
}

void usbProtoInit()
{
	memset(&protoStatus, 0, sizeof(protoStatus));
	memset(packBuffer, 0, sizeof(packBuffer));
	SP_Clear(&currPacket);
	SP_AttachBuffer(&currPacket, packBuffer, 255);
	rxState = WAIT_START_FRAME;
	resetCrc();
}

/**
 * @brief transferTimeout
 *  Pack receive timeout
 */
void usbTransferTimeout()
{
    if(rxState != WAIT_START_FRAME) {
        rxState = FRAME_ERROR;
        protoStatus.rxTimeouts++;
        protoStatus.rxTotalErrors++;
    }
}

/**
 *  @brief usbHostOnlineTimeout
 *  Lose connection with host
 */
void usbHostOnlineTimeout()
{
	protoStatus.fHostOnline = 0;
}

/**
 *	@brief usbReceiveData
 *	New data for handling
 */
void usbReceiveData(uint8_t *data, uint8_t len)
{
	 //побайтный разбор данных
	while(len--) {
		uint8_t c = *data++;
		protoStatus.rxBytes++;
		dataHandler(c);
	}
}

/**
 * @brief usbSendPacket
 * Отправка пакета данных в последовательный порт
 * (Отправляются только валидные пакеты)
 * Выполняется байт-стаффинг здесь ((С)Йода)
 * @param pack пакет для отправки
 * 		  destBuff буфер отправки (место откуда данные будут затем считаны)
 * return -1 в случае ошибки формирования данных, иначе количество байт для передачи (с учетом стаффинга)
 */
int usbSendPacket(SerialPacket *pack, uint8_t *destBuff)
{
    if(!destBuff) {
    	return -1;
    }
    uint8_t *origPtr = destBuff;
	if(SP_isValid(pack)) {
		*destBuff++ = FSTART;
        *destBuff++ = pack->address;
        *destBuff++ = pack->command;
        uint8_t stuffByte = tryToStuffByte(pack->length);
        if(stuffByte != pack->length) {
        	*destBuff++ = FESC;
        	*destBuff++ = stuffByte;
        }
        else {
        	*destBuff++ = pack->length;
        }
        if(pack->length && pack->data) {
            for(uint8_t index = 0; index < pack->length; index++) {
                stuffByte = tryToStuffByte(pack->data[index]);
                if(stuffByte != pack->data[index]) {
                	*destBuff++ = FESC;
                	*destBuff++ = stuffByte;
                }
                else {
                	*destBuff++ = pack->data[index];
                }
            }
        }
        uint16_t crc16 = computeCRC(pack);
        uint8_t *crc = (uint8_t *) &crc16;
        for(int i = 0; i < 2; i++) {
            stuffByte = tryToStuffByte(crc[i]);
            if(stuffByte != crc[i]) {
            	*destBuff++ = FESC;
            	*destBuff++ = stuffByte;
            }
            else {
                *destBuff++ = crc[i];
            }
        }
    	uint32_t byteCount = destBuff - origPtr;
    	protoStatus.txBytes += byteCount;
    	protoStatus.txPacks++;
    }
	return (int)(destBuff - origPtr);
}

/**
 * External (weak) functions
 */

void __attribute__((weak)) usbPackReceived(SerialPacket *pack)
{

}

/**
 * Private methods
 */

static void dataHandler(uint8_t c)
{
	switch (rxState) {
	case WAIT_START_FRAME:
		rxState = checkStartMark(c);
		break;
	case WAIT_ADDRESS:
		rxState = checkAddress(c);
		break;
	case WAIT_CMD:
		rxState = checkCmd(c);
		break;
	case WAIT_LEN:
		rxState = checkLen(c);
		break;
	case DATA_FLOW:
		rxState = checkData(c);
		break;
	case STUFFING:
		rxState = doSomeStuff(rxState, c);
		break;
	case WAIT_CRC:
		rxState = checkCrc(c);
		break;
	case WAIT_CRC_EMPTY_PACK:
		rxState = checkCrc(c);
		break;
	case FRAME_ERROR:
		resetCrc();
		resetRxBuffer();
		rxState = checkStartMark(c);
		break;
	case STUFF_LEN:
		rxState = doSomeStuff(rxState, c);
		break;
	case STUFF_CRC:
		rxState = doSomeStuff(rxState, c);
		break;
	default:
		break;
	}
}

static ReceiverState checkStartMark(uint8_t byte)
{
    if(byte == FSTART) {
        return WAIT_ADDRESS;
    }
    return FRAME_ERROR;
}

static ReceiverState checkAddress(uint8_t byte)
{
    if(byte & 0x80) {
        return FRAME_ERROR;
    }
    currPacket.address = byte;
    return WAIT_CMD;
}

static ReceiverState checkCmd(uint8_t byte)
{
    if(byte & 0x80) {
        return FRAME_ERROR;
    }
    currPacket.command = byte;
    return WAIT_LEN;
}

static ReceiverState checkLen(uint8_t byte)
{
    ReceiverState state = FRAME_ERROR;
    //байт стаффинг
    if(byte == FESC) {
        state = STUFF_LEN;
    }
    else if(byte == FSTART) {
    	protoStatus.rxFormatErrors++;
    	protoStatus.rxTotalErrors++;
    }
    //стартовая метка не должна быть здесь
    else {
        currPacket.length = byte;
        if(currPacket.length) {
            state = DATA_FLOW;
        }
        //пустой пакет
        else {
            state = WAIT_CRC_EMPTY_PACK;
        }
    }
    return state;
}

static ReceiverState checkData(uint8_t byte)
{
    ReceiverState state = FRAME_ERROR;
    if(byte == FESC) {
        state = STUFFING;
    }
    else if(byte == FSTART) {
    	protoStatus.rxFormatErrors++;
    	protoStatus.rxTotalErrors++;
    }
    else {
        *currPacket.head++ = byte;
        if(currPacket.data + currPacket.length == currPacket.head) {
            state = WAIT_CRC;
        }
        else {
            state = DATA_FLOW;
        }
    }
    return state;
}

static ReceiverState doSomeStuff(ReceiverState state, uint8_t byte)
{
    ReceiverState res = FRAME_ERROR;
    int fStuff = 0;
    uint8_t stuffByte = 0x00;
    switch (byte) {
    case TFSTART:
        stuffByte = FSTART;
        fStuff = 1;
        break;
    case TFEND:
        stuffByte = FESC;
        fStuff = 1;
        break;
    default:
    	protoStatus.rxFormatErrors++;
    	protoStatus.rxTotalErrors++;
        break;
    }
    //perform byte stuffing
    if(fStuff) {
        //and where we need to stuff
        switch (state) {
        //stuff in data
        case STUFFING:
            *currPacket.head++ = stuffByte;
            if(currPacket.data + currPacket.length == currPacket.head) {
                res = WAIT_CRC;
            }
            else {
                res = DATA_FLOW;
            }
            break;
        //stuff in pack length
        case STUFF_LEN:
            currPacket.length = stuffByte;
            res = DATA_FLOW;
            break;
        //stuff in pack CRC
        case STUFF_CRC:
        	if(crcByteNum == 0) {
        		currCrc = stuffByte;
        		crcByteNum++;
        		res = WAIT_CRC;
        	}
        	else {
        		res = finalCrcCheck(stuffByte);
        	}
            break;
        default:
            break;
        }
    }
    return res;
}

static ReceiverState checkCrc(uint8_t byte)
{
    ReceiverState res = FRAME_ERROR;
    switch (byte) {
    case FSTART:
        break;
    case FESC:
        res = STUFF_CRC;
        break;
    default:
    	//first byte of crc16
    	if(crcByteNum == 0) {
    		currCrc = byte;
    		crcByteNum++;
    		res = WAIT_CRC;
    	}
    	//ready to check crc16
    	else {
    		res = finalCrcCheck(byte);
    	}
        break;
    }
    return res;
}

static ReceiverState finalCrcCheck(uint8_t byte)
{
    ReceiverState res = FRAME_ERROR;
    currCrc |= (byte << 8);
    //CRC OK
    if(computeCRC(&currPacket) == currCrc) {
    	usbPackReceived(&currPacket);
    	res = WAIT_START_FRAME;
    	protoStatus.rxPacks++;
    	protoStatus.fHostOnline = 1;
    }
    //CRC BAD
    else {
    	protoStatus.rxTotalErrors++;
    	protoStatus.rxCrcErrors++;
    }
    resetRxBuffer();
    resetCrc();
    return res;
}

static uint16_t computeCRC(const SerialPacket *pack)
{
    uint16_t crc = CRC16_CCITT_INIT;
    if(pack) {
    	crc = crc16_ccitt_byte(crc, FSTART);
    	crc = crc16_ccitt_byte(crc, pack->address);
    	crc = crc16_ccitt_byte(crc, pack->command);
    	crc = crc16_ccitt_byte(crc, pack->length);
    	if(pack->length && pack->data) {
    		crc = crc16_ccitt(crc, pack->data, pack->length);
    	}
    }
    //invert final checksum
    return ~crc;
}

static void resetRxBuffer()
{
	SP_Clear(&currPacket);
	SP_AttachBuffer(&currPacket, packBuffer, 255);
}

static void resetCrc()
{
	crcByteNum = 0;
	currCrc = 0;
}

/**
 * @brief tryToStuffByte
 *  Попытка выполнить байт-стаффинг. Если БС не нужен, то
 * возвращается тот же самый байт, иначе его замена (без FESC)
 * @param byte байт данных
 * @return байт данных с учетом байт-стаффинга
 */
static uint8_t tryToStuffByte(uint8_t byte)
{
    if(byte == FSTART) {
        return TFSTART;
    }
    else if(byte == FESC) {
        return TFEND;
    }
    else {
        return byte;
    }
}
