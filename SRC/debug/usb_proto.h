/*
 * usb_proto.h
 *
 *  Created on: 14 янв. 2018 г.
 *      Author: dm
 */

#ifndef SRC_DEBUG_USB_PROTO_H_
#define SRC_DEBUG_USB_PROTO_H_

#define USB_PROTO_BUFFER_SIZE 256

typedef struct {
	uint8_t address;
	uint8_t command;
	uint8_t length;
	uint8_t *data;
	uint8_t *head;
} SerialPacket;

void SP_Init(SerialPacket *pack, uint8_t addr, uint8_t cmd, uint8_t len);
void SP_AttachBuffer(SerialPacket *pack, uint8_t *buffer, uint8_t size);
void SP_Clear(SerialPacket *pack);
int SP_isValid(SerialPacket *pack);

typedef enum {
	FRAME_ERROR,
	WAIT_START_FRAME,
	WAIT_ADDRESS,
	WAIT_CMD,
	WAIT_LEN,
	DATA_FLOW,
	WAIT_CRC,
	WAIT_CRC_EMPTY_PACK,
	STUFFING,
	STUFF_LEN,
	STUFF_CRC
} ReceiverState;

/**
 * @brief The Abonents enum
 *  Адреса абонентов канала
 */
typedef enum {
	ADDR_BROAD   		= 0x00,		//широковещательный адрес
	ADDR_HOST    		= 0x01,    	//мастер канала (хост)
	ADDR_TAB_VISION    	= 0x02,  	//устройство TabVision
} AbonentAddress;

typedef struct {
	uint32_t rxPacks;
	uint32_t txPacks;
	uint32_t rxBytes;
	uint32_t txBytes;
	uint32_t rxTotalErrors;
	uint32_t rxTimeouts;
	uint32_t rxCrcErrors;
	uint32_t rxFormatErrors;
	short fHostOnline;
} USBProtoStatus_t;

void usbProtoInit();
void usbTransferTimeout();
void usbHostOnlineTimeout();
USBProtoStatus_t *usbProtoStatus();

void usbReceiveData(uint8_t *data, uint8_t len);
int usbSendPacket(SerialPacket *pack, uint8_t *destBuff);

//something like Qt's signal
void usbPackReceived(SerialPacket *pack);


#endif /* SRC_DEBUG_USB_PROTO_H_ */
