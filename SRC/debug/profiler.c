/*
 * profiler.c
 *
 * Simple profiler, based on SysTick
 *
 *  Created on: 25 апр. 2018 г.
 *      Author: leshhev_ds
 */

#include "../Libraries/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h"

#include "profiler.h"

void initProfiler()
{
	SysTick->LOAD = 0xFFFFFF - 1;
	SysTick->VAL = 0;
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
}

void startProfiler(ProfileInfo_t *info)
{
	info->result = 0;
	info->startTicks = SysTick->VAL;
}

void stopProfiler(ProfileInfo_t *info)
{
	info->result = (info->startTicks - SysTick->VAL) & 0xFFFFFF;
}
