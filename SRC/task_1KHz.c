/*
 * task_1KHz.c
 *
 *	Main loop.
 *
 *  Created on: 22 нояб. 2017 г.
 *      Author: leshhev_ds
 */

#include "main.h"

volatile uint32_t CNT_1kHz = 0;
volatile uint32_t CNT_1Hz = 0;

volatile short fMainLoopStep = 0;

static void updateCounters();
static void processInternalTempSensor();

static int32_t sensorTemperature = -273;
static uint16_t hiberCode;
static uint8_t fHiberStart = 0;

static SimpleTimer_t ledTimer;
static SimpleTimer_t temperatureTimer;

void task_1KHz()
{
	timerStart(&ledTimer, 500);
	timerStart(&temperatureTimer, 500);

	uint8_t ledState = 0x1;
	uint16_t buttonCounter = 0;

	//infinite loop
	for(;;) {
		if(fMainLoopStep == 0) {
			asm volatile("nop");
			continue;
		}

		updateCounters();
		if(fHiberStart == 1) {
			hiberProcess();
		}
		debugProcess();

		//just to be sure that application is running OK
		if(timerExpired(&ledTimer)) {
			timerStart(&ledTimer, 500);
			setPin(BOARD_LED_LD3, ledState);
			ledState ^= 0x1;

			if(getPin(BOARD_USER_BUTTON)) {
				buttonCounter++;
			}
			else {
				if(buttonCounter >= 2) {
					usb_printf("wake up\r\n");
					fHiberStart = 1;
					//hbWakeUp(1);
				}
				/*else {
					hbWakeUp(0);
				}*/
				buttonCounter = 0;
			}

			/*if(fHiberCheckCode == 1) {
				if(hiberCode == HIBER_RESPONSE_OK) {
					usb_printf("gps_mode_off\r\n");
				}
				fHiberCheckCode = 0;
			}

			if(hbState() != HIBER_STATE_OFFLINE) {
				hbSetGpsMode(0, &hiberCode);
				fHiberCheckCode = 1;
			}*/

		}

		if(timerExpired(&temperatureTimer)) {
			timerStart(&temperatureTimer, 500);
			processInternalTempSensor();
		}

		fMainLoopStep = 0;
	}
}

int32_t getInternalSensorTemperature()
{
	return sensorTemperature;
}

/**
 * User definition of essential HiberBand modem functions
 */

void hbWrite(uint8_t* str)
{
	uartWriteStr(str);
}

int hbRead(uint8_t* buffer, size_t maxLen)
{
	return uartReadAll(buffer, maxLen);
}

void hbWakeUp(uint8_t flag)
{
	//wake up modem
	if(flag) {
		setPinTrue(HIBER_WAKE_UP);
		//turn on the board led
		setPinTrue(BOARD_LED_LD4);
	}
	else {
		setPinFalse(HIBER_WAKE_UP);
		//turn off the board led
		setPinFalse(BOARD_LED_LD4);
	}
}

void hbReset(uint8_t flag)
{
	//reset modem
	if(flag) {
		setPinTrue(HIBER_RESET);
	}
	else {
		setPinFalse(HIBER_RESET);
	}
}

void hbSetOnlineLed(uint8_t flag)
{
	if(flag) {
		setPinTrue(BOARD_LED_LD5);
	}
	else {
		setPinFalse(BOARD_LED_LD5);
	}
}

void hbSetDataLed(uint8_t flag)
{
	if(flag) {
		setPinTrue(BOARD_LED_LD6);
	}
	else {
		setPinFalse(BOARD_LED_LD6);
	}
}

/**
 * Private section
 */

static void updateCounters()
{
	static uint32_t counter = 0;
	CNT_1kHz++;
	counter++;
	if(counter == 1000) {
		counter = 0;
		CNT_1Hz++;
	}
}

/**
 * ADC test internal temperature sensor
 */

#define TEMP_SENSOR_AVG_SLOPE_MV_PER_CELSIUS                        2.5f
#define TEMP_SENSOR_VOLTAGE_MV_AT_25                                760.0f
#define ADC_REFERENCE_VOLTAGE_MV                                    3300.0f
#define ADC_MAX_OUTPUT_VALUE                                        4095.0f
#define TEMP110_CAL_VALUE                                           ((uint16_t*)((uint32_t)0x1FFF7A2E))
#define TEMP30_CAL_VALUE                                            ((uint16_t*)((uint32_t)0x1FFF7A2C))
#define TEMP110                                                     110.0f
#define TEMP30                                                      30.0f
#define TEMP_MAX_MEASURE_COUNT										100

static uint32_t tempMeasureCount = 0;
static int32_t tempSum;

static void processInternalTempSensor()
{
	//Start the conversion
	ADC_SoftwareStartConv(ADC1);
	//Processing the conversion
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	//Return the converted data
	uint32_t result = ADC_GetConversionValue(ADC1);

	int32_t temperature = (int32_t)((TEMP110 - TEMP30) / ((float)(*TEMP110_CAL_VALUE) - (float)(*TEMP30_CAL_VALUE))
									* (result - (float)(*TEMP30_CAL_VALUE)) + TEMP30);

	tempSum += temperature;
	tempMeasureCount++;

	sensorTemperature = tempSum / tempMeasureCount;

	if(tempMeasureCount >= TEMP_MAX_MEASURE_COUNT) {
		tempMeasureCount = 0;
		tempSum = 0;
	}
}
