/*
 * config.h
 *
 *  Created on: 16 июл. 2019 г.
 *      Author: dm
 */

#ifndef SRC_CONFIG_H_
#define SRC_CONFIG_H_

#define USE_USB_OTG_FS						//micro usb
#define MAIN_TIMER_FREQ_KHZ 10				//main timer frequency in khz


#endif /* SRC_CONFIG_H_ */
