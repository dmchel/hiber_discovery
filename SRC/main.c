/**
 * Test program for STM32F4 Discovery board and Hiber LPGAN modem connection
 */

#include "main.h"

void init_NVIC()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_SetPriority(TIM3_IRQn, 0);
	NVIC_EnableIRQ(TIM3_IRQn);

	NVIC_SetPriority(USART3_IRQn, 1);
	NVIC_EnableIRQ(USART3_IRQn);

	//NVIC_SetPriority(DMA1_Stream1_IRQn, 1);
	//NVIC_EnableIRQ(DMA1_Stream1_IRQn);

	//NVIC_SetPriority(DMA1_Stream3_IRQn, 2);
	//NVIC_EnableIRQ(DMA1_Stream3_IRQn);
}

void initADC()
{
	ADC_DeInit();

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	ADC_CommonInitTypeDef ADC_CommonInitStruct;
	ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStruct);

	ADC_InitTypeDef ADC_InitStruct;
	ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStruct.ADC_ScanConvMode = DISABLE;
	ADC_InitStruct.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStruct.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_InitStruct);

	// ADC1 Configuration, ADC_Channel_TempSensor is actual channel 16
	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1,
							 ADC_SampleTime_144Cycles);

	// Enable internal temperature sensor
	ADC_TempSensorVrefintCmd(ENABLE);

	// Enable ADC conversion
	ADC_Cmd(ADC1, ENABLE);
}

int main(void)
{
	__disable_irq();

	SystemCoreClockUpdate();
	initProfiler();
	initTimers(MAIN_TIMER_FREQ_KHZ);
	initUart();
	initIO();
	initUsbDebug();
	initADC();

	//enable random generator clock
	//RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
	init_NVIC();

	__enable_irq();

	task_1KHz();
	//never return from task_1KHz()
  	return 0;
}
