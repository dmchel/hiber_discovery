/*
 * queue.c
 *
 *  Created on: 02.10.2012
 *      Author: Lubimov
 */

#include <string.h>

#include "queue.h"

void queue_init(queue_t* queue, void* buf, unsigned int buf_size, unsigned int data_size)
{
	queue->buf = buf;
	queue->buf_size = buf_size;
	queue->data_size = data_size;
	queue->tail = buf;
	queue->count = 0;
	queue->max_count = buf_size / data_size;
}

int queue_is_full(queue_t* queue)
{
	return queue->count >= queue->max_count;
}

int queue_is_empty(queue_t* queue)
{
	return queue->count == 0;
}

int queue_put(queue_t* queue, void* item)
{
	if (queue_is_full(queue))
	{
		return 0;
	}

	unsigned char* head;

	/* Compute the last place in the queue. */
	head = (unsigned char*)queue->tail - (queue->count * queue->data_size);
	if (head < (unsigned char*)queue->buf)
	{
		head += queue->buf_size;
	}

	/* Put the packet in. */
	unsigned int l = queue->data_size / sizeof(unsigned char);
	memcpy(head, item, l);
	queue->count++;
	return 1;
}

int queue_get(queue_t* queue, void* item)
{
	if (queue_is_empty(queue))
	{
		return 0;
	}

	/* Get the first packet from queue. */
	unsigned int l = queue->data_size / sizeof(unsigned char);
	memcpy(item, queue->tail, l);

	/* Advance head pointer. */
	queue->tail = (unsigned char*)queue->tail - queue->data_size;
	if ((unsigned char*)queue->tail < (unsigned char*)queue->buf)
	{
		queue->tail = (unsigned char*)queue->tail + queue->buf_size;
	}
	queue->count--;
	return 1;
}

void queue_clear(queue_t* queue)
{
	queue->tail = queue->buf;
	queue->count = 0;
}
