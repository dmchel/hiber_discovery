/*
 * queue.h
 *
 *  Created on: 02.10.2012
 *      Author: Lubimov
 */

#ifndef QUEUE_H_
#define QUEUE_H_


typedef struct
{
	unsigned int count;
	void* tail;
	void* buf;
	unsigned int buf_size;
	unsigned int data_size;
	unsigned int max_count;
} queue_t;

void queue_init(queue_t* queue, void* buf, unsigned int buf_size, unsigned int data_size);
int queue_is_full(queue_t* queue);
int queue_is_empty(queue_t* queue);
int queue_put(queue_t* queue, void* item);
int queue_get(queue_t* queue, void* item);
void queue_clear(queue_t* queue);

#endif /* QUEUE_H_ */
